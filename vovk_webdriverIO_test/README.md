# Vovk test

### automated test for a site Vovk on js using webdriverIO

## Test Cases:

1. **Navigation Test —** Verify that a user is able to navigate between pages on the website.

### Steps:

- Navigate to the home page
- Click on a button to navigate to "Our studios" page
- Verify that the user is redirected to the correct page and that the page loads successfully
- Click on a button to navigate back to the home page
- Verify that the user is redirected back to the home page and that the page loads successfully

2. **Search Test —** Verify that a user is able to search for and find a specific item on the website.

### Steps:

- Navigate to the home page
- Enter a search term in the search bar
- Click on the Search button
- Verify that the search results page loads successfully and displays relevant search results
- Click on one of the search results
- Verify that the user is redirected to the correct product page and that the page loads successfully

3. **Add to Cart Test —** Verify that a user is able to add items to their cart and complete the checkout process.

### Steps:

- Navigate to the home page
- Click on a product to view its details
- Click on the "Add to Cart" button
- Navigate to the cart page
- Verify that the added product is displayed in the cart with the correct details (e.g. name, price, quantity, etc.)
- Click on the "Checkout" button
- Enter some shipping and payment details
- Click on the "Place Order" button
- Verify that the order is successfully placed and that the user is redirected to an order confirmation page

4. **Remove from** **Cart Test —** Verify that a user is able to remove a product from their shopping cart by clicking on the "Remove" button.

### Steps:

- Navigate to the home page
- Click on a product to view its details
- Click on the "Add to Cart" button
- Navigate to the cart page - Verify that the added product is displayed in the cart with the correct details (e.g. name, price, quantity, etc.)
- Increase the quantity of the product in the cart by clicking the "+" button
- Verify that the quantity and total price have updated
- Remove product from the cart
- Verify that product has been removed

5. **Form Validation Test —** Verify that user input is validated and error messages are displayed appropriately for invalid input.

### Steps:

- Navigate to a page with a form that requires user input (e.g. registration page)
- Enter invalid data in one or more fields (e.g. invalid email format)
- Attempt to submit the form - Verify that error messages are displayed next to the invalid fields with specific error messages (e.g. "Invalid email format")
- Correct the invalid data and re-submit the form - Verify that the form is successfully submitted and the user is redirected to the appropriate page

## how to run the test:

- clone the project to your computer
- install dependencies using the command `npm install`
- run test using the command `npx wdio`

## test run example

![Photo](vovk_test_result.jpg)
