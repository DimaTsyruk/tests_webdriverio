class BlackkirtPage {
  get addToTheCartButton() {
    return $('//*[@id="button-cart"]');
  }

  get lSize() {
    return $('label[for="prod-option-1031442896"]');
  }

  async addToTheCart() {
    await this.addToTheCartButton.click();
  }

  async chooseSize() {
    await this.lSize.click();
  }
}

export default new BlackkirtPage();
