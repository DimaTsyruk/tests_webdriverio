import { equal } from 'assert';

class MainPage {
  async verifyURL(expectedUrl) {
    const actualUrl = await browser.getUrl();
    equal(actualUrl, expectedUrl, 'URL is incorrect');
  }
}
export default new MainPage();
