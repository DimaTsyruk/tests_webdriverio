import { equal } from 'assert';

class CheckoutPage {
  get nameField() {
    return $('//*[@id="input-payment-firstname"]');
  }

  get surnameField() {
    return $('//*[@id="input-payment-lastname"]');
  }

  get phoneField() {
    return $('//*[@id="input-payment-telephone"]');
  }

  get emailField() {
    return $('//*[@id="input-payment-email"]');
  }

  get checkoutButton() {
    return $('//*[@id="tmdbuttonorder"]');
  }

  get delateButton() {
    return $('//div[@class="tmdcart__delete"]');
  }

  async enterName(name) {
    await this.nameField.setValue(name);
  }

  async enterSurame(surname) {
    await this.surnameField.setValue(surname);
  }

  async enterPhone(phone) {
    await this.phoneField.setValue(phone);
  }

  async enterEmail(email) {
    await this.emailField.setValue(email);
  }

  async confirmOrder() {
    await this.checkoutButton.click();
  }

  async removeProduct() {
    await this.delateButton.click();
  }
}

export default new CheckoutPage();
