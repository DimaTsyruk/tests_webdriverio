import { equal } from 'assert';

class HomePage {
  get ourStudiosButton() {
    return $('//div[@class="top-header__menu"]//a[contains(text(), "Наші студії")]');
  }

  get logoButton() {
    const elements = $$('img[title="VOVK"]');
    return elements[0];
  }

  get magnifierВutton() {
    return $('div[title="Пошук"]');
  }

  get searchField() {
    const elements = $$('input[name="search"]');
    return elements[0];
  }

  get firstProduct() {
    return $('//div[@class="iSearchItem"]');
  }

  get firstNewProduct() {
    return $('//div[@class="product__body-block"]');
  }

  get nameOfProduct() {
    return $('//*[@id="product"]//h1');
  }

  async open() {
    await browser.url('https://vovk.com/ua/');
  }

  async navigateToOurStudios() {
    await this.ourStudiosButton.click();
  }

  async returnToHomePage() {
    await this.logoButton.click();
  }

  async openSearchField() {
    await this.magnifierВutton.click();
  }

  async searchProduct(productName) {
    await this.searchField.setValue(productName);
  }

  async navigateToSelectedProduct() {
    await browser.pause(1000);
    await this.firstProduct.click();
  }

  async navigateToNewProduct() {
    await this.firstNewProduct.scrollIntoView();
    await this.firstNewProduct.click();
    await browser.pause(2000);
  }

  async verifyNameOfProduct(expectedName) {
    const name = await this.nameOfProduct.getText();
    equal(name, expectedName, 'name is not correct');
  }
}

export default new HomePage();
