import { equal } from 'assert';

class CartPage {
  get nameOfProduct() {
    return $('//tr//div[2]/a');
  }

  get quantityOfProduct() {
    return $('input[class="cart__input-qty"]');
  }

  get priceOfProduct() {
    return $('//span[@class="cart__price"]');
  }

  get checkoutButton() {
    return $('//a[contains(text(), "Оформити покупку")]');
  }

  get plusButton() {
    const elements = $$('//*[@id="add-plus-button"]');
    return elements[1];
  }

  get removeButton() {
    const elements = $$('//*[@id="close-button"]');
    return elements[1];
  }

  get cartMassege() {
    return $('//p[@class="text-center"]');
  }

  async verifyNameOfProduct(expectedName) {
    const name = await this.nameOfProduct.getText();
    equal(name, expectedName, 'wrong name');
  }

  async verifyQuantityOfProduct(expectedQuantity) {
    const quantity = await this.quantityOfProduct.getValue();
    equal(quantity, expectedQuantity, 'wrong quantity');
  }

  async verifyPriceOfProduct() {
    const expectedPrice = '1390';
    const price = await this.priceOfProduct.getValue();
    equal(price, expectedPrice, 'wrong price');
  }

  async makePurchase() {
    await this.checkoutButton.click();
  }

  async increaseQuantity() {
    await this.plusButton.click();
  }

  async removeProduct() {
    await this.removeButton.click();
  }

  async checkIsCartEmpty() {
    const quantity = await this.cartMassege.getText();
    const expectedQuantity = 'У кошику порожньо!';
    equal(quantity, expectedQuantity, 'cart is not empty');
  }
}

export default new CartPage();
