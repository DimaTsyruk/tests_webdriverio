import { equal } from 'assert';

class LoginPage {
  get phoneField() {
    return $('//*[@id="input-telephone"]');
  }

  get passwordField() {
    return $('//*[@id="input-password"]');
  }

  get loginButton() {
    return $('//input[@value="Ввійти"]');
  }

  get errorMessge() {
    return $('div[class="alert-danger brainlab__alert"]');
  }

  async open() {
    await browser.url('https://vovk.com/ua/login-page/');
  }

  async enterPhone(phone) {
    await this.phoneField.setValue(phone);
  }

  async enterPssword(password) {
    await this.passwordField.setValue(password);
  }

  async login() {
    await this.loginButton.click();
  }

  async checkTheErrorMessage(expectedMessage) {
    const message = await this.errorMessge.getText();
    equal(message, expectedMessage, 'error message is not displayed');
  }
}

export default new LoginPage();
