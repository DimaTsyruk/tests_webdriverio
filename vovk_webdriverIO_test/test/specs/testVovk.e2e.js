import BlackSkirtPage from '../pageobjects/blackSkirtPage.js';
import HomePage from '../pageobjects/homePage.js';
import CartPage from '../pageobjects/cartPage.js';
import CheckoutPage from '../pageobjects/checkoutPage.js';
import LoginPage from '../pageobjects/loginPage.js';
import MainPage from '../pageobjects/mainPage.js';

describe('Vovk Navigation', () => {
  it('should open the home page', async () => {
    await HomePage.open();
  });

  it('should navigate to "Our studios" page', async () => {
    await HomePage.navigateToOurStudios();
    await MainPage.verifyURL('https://vovk.com/ua/studio/');
  });

  it('should return to home page', async () => {
    await HomePage.returnToHomePage();
    await MainPage.verifyURL('https://vovk.com/ua/');
  });
});

describe('Vovk Search ', () => {
  it('should open the home page', async () => {
    await HomePage.open();
  });

  it('should search black skirt', async () => {
    await HomePage.openSearchField();
    await HomePage.searchProduct('Чорна спідниця');
  });

  it('should navigate to selected product', async () => {
    await HomePage.navigateToSelectedProduct();
    await HomePage.verifyNameOfProduct('Спідниця міні еко-замша чорна');
  });
});

describe('Vovk Adding to the cart ', () => {
  it('should open the home page', async () => {
    await HomePage.open();
  });

  it('should navigate to "Shirt with pockets staple lime"', async () => {
    await HomePage.navigateToNewProduct();
  });

  it('should add product to the cart', async () => {
    await BlackSkirtPage.chooseSize();
    await BlackSkirtPage.addToTheCart();
  });

  it('should check correctly added', async () => {
    await CartPage.verifyNameOfProduct('Жакет костюмна тканина гусяча лапка блакитно-молочна');
    await CartPage.verifyQuantityOfProduct(1);
  });

  it('should navigate to checkout pge', async () => {
    await CartPage.makePurchase();
  });

  it('should enter payment details', async () => {
    await CheckoutPage.enterName('Дмитро');
    await CheckoutPage.enterSurame('Шевченко');
    await CheckoutPage.enterPhone('0501234567');
    await CheckoutPage.enterEmail('aaa@ukr.net');
  });

  it('should confirm order', async () => {
    await CheckoutPage.confirmOrder();
  });

  it('check if url is correct', async () => {
    await MainPage.verifyURL('https://vovk.com/ua/buy/');
  });

  it('should remove product from the cart', async () => {
    await CheckoutPage.removeProduct();
  });
});

describe('Vovk Removing from cart', () => {
  it('should open the home page', async () => {
    await HomePage.open();
  });

  it('should navigate to new product page', async () => {
    await HomePage.navigateToNewProduct();
  });

  it('should add product to the cart', async () => {
    await BlackSkirtPage.chooseSize();
    await BlackSkirtPage.addToTheCart();
  });

  it('should check correctly added', async () => {
    await CartPage.verifyNameOfProduct('Жакет костюмна тканина гусяча лапка блакитно-молочна');
    await CartPage.verifyQuantityOfProduct(1);
  });

  it('should increase the quantity of goods', async () => {
    await CartPage.increaseQuantity();
    await CartPage.verifyQuantityOfProduct(2);
  });

  it('should remove product from the cart', async () => {
    await CartPage.removeProduct();
    await CartPage.checkIsCartEmpty();
  });
});

describe('Vovk Validation ', () => {
  it('should open the login page', async () => {
    await LoginPage.open();
  });

  it('should enter invalid data', async () => {
    await LoginPage.enterPhone('1234');
    await LoginPage.enterPssword('Qwerty123');
  });

  it('should try login with invalid data', async () => {
    await LoginPage.login();
  });

  it('should verify that error messages are displayed', async () => {
    await LoginPage.checkTheErrorMessage('Неправильно заповнені поля Телефон і / або пароль!');
  });

  it('should enter correctly data', async () => {
    await LoginPage.enterPhone('501234567');
    await LoginPage.enterPssword('Qwerty123');
  });

  it('should try login with corectly data', async () => {
    await LoginPage.login();
  });

  it('should verify that error messages are displayed', async () => {
    await LoginPage.checkTheErrorMessage('Неправильно заповнені поля Телефон і / або пароль!');
  });
});
