# tests_webdriverIO

### This repository contains tests written in JavaScript using the webdriverIO framework

Test cases, instructions for starting the project and examples of how tests work are in the README.md file inside the folder with the test
