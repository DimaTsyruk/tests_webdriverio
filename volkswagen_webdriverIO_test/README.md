# Volkswagen website test

### automated test for a site Volkswagen on JS using webdriverIO

## Test Cases:

1. **Navigation Test —** Verify that a user is able to navigate between pages on the website.

### Steps:

- Navigate to the home page
- Click on a menu button
- Click on a button to navigate to the page ("Models", "Builder", "Offers", "myVW" and "Contact Us")
- Verify that the user is redirected to the correct page and that the page loads successfully
- Click on a button to navigate back to the menu page

2. **Laguage Change Test —** — Verify that a user is able to change site language.

### Steps:

- Navigate to the home page
- Verify that the language of the site is English
- Click on the "español" button to change the language
- Verify that the language of the site is Spanish

3. **Search Test —** Verify that the car can be found using the search bar.

### Steps:

- Navigate to the home page
- Сlick on the magnifying glass button
- Enter the product name in the search bar (e.g."tiguan")
- Click on tiguan model button
- Verify that the user is redirected to the correct page and that the page loads success

4. **Login test** **Cart Test —** Verify that the input field does not accept invalid mail.

### Steps:

- Navigate to the home page
- Navigate to the user account
- Click on the "Log in or register" button
- Enter an email in a non-existent format (e.g. "abc123")
- Verify thaterror message is displayed
- Enter correct email (e.g. "abc123@gmail.com")
- Click on the "Next" button

## how to run the test:

- clone the project to your computer
- install dependencies using the command `npm install`
- run test using the command `npx wdio`

## test run example

![Photo](volkswagen_test_result.jpg)

![Video](volkswagen_test_result.mp4)
