import HomePage from '../pageobjects/homePage.js';
import MainPage from '../pageobjects/mainPage.js';
import MenuPage from '../pageobjects/menuPage.js';
import SearchPage from '../pageobjects/searchPage.js';
import LoginPage from '../pageobjects/loginPage.js';

describe('Navigation Test', () => {
  it('should open the home page', async () => {
    await MainPage.open();
  });

  it('should close the popup window ', async () => {
    await HomePage.cancelCookie();
  });

  it('should navigate to the menu page', async () => {
    await HomePage.navigateToMenu();
  });

  it('should navigate to the models page', async () => {
    await MenuPage.navigateToModelsPage();
    await MainPage.verifyURL('https://www.vw.com/en/models.html');
    await HomePage.navigateToMenu();
  });

  it('should navigate to the models page', async () => {
    await MenuPage.navigateToModelsPage();
    await MainPage.verifyURL('https://www.vw.com/en/models.html');
    await HomePage.navigateToMenu();
  });

  it('should navigate to the builder page', async () => {
    await MenuPage.navigateToBuilderPage();
    await MainPage.verifyURL('https://www.vw.com/en/builder.html');
    await HomePage.navigateToMenu();
  });

  it('should navigate to the offers page', async () => {
    await MenuPage.navigateToOffersPage();
    await MainPage.verifyURL('https://www.vw.com/en/offers.html');
    await HomePage.navigateToMenu();
  });

  it('should navigate to the myVW page', async () => {
    await MenuPage.navigateToMyVWPage();
    await MainPage.verifyURL('https://www.vw.com/en/owners.html');
    await HomePage.navigateToMenu();
  });

  it('should navigate to the contact us page', async () => {
    await MenuPage.navigateToContactUsPage();
    await MainPage.verifyURL('https://www.vw.com/en/contact.html');
    await HomePage.navigateToMenu();
  });
});

describe('Laguage Change Test', () => {
  it('should open the home page', async () => {
    await MainPage.open();
  });

  xit('should close the popup window ', async () => {
    await HomePage.cancelCookie();
  });

  it('should navigate to the menu page', async () => {
    await HomePage.navigateToMenu();
  });

  it('verify that the language of the site is English', async () => {
    await MainPage.verifyURL('https://www.vw.com/en.html');
  });

  it('should change the language', async () => {
    await MenuPage.changeLanguage();
  });

  it('verify that the language of the site is Spanish', async () => {
    await MainPage.verifyURL('https://www.vw.com/es/');
  });
});

describe('Search Test', () => {
  it('should open the home page', async () => {
    await MainPage.open();
  });

  xit('should close the popup window ', async () => {
    await HomePage.cancelCookie();
  });

  it('should search a product', async () => {
    await HomePage.openSearchBar();
    await SearchPage.searchProduct('tiguan');
  });

  it('should navigate to the Tiguan page', async () => {
    await SearchPage.builtTiguan();
  });

  it('should vefify page', async () => {
    await SearchPage.verifyPage('Tiguan');
  });
});

describe('Login Test', () => {
  it('should open the home page', async () => {
    await MainPage.open();
  });

  xit('should close the popup window ', async () => {
    await HomePage.cancelCookie();
  });

  it('should navigate to login page ', async () => {
    await HomePage.navigateToLoginPage();
    await MainPage.verifyURLbyKeyWord('signin-service');
  });

  it('should enter an incorrect email', async () => {
    await LoginPage.enterEmail('abc123');
  });

  it('should verify that the error message is displayed', async () => {
    await LoginPage.checkErrorEmailMessage();
  });

  it('should enter an correct email', async () => {
    await LoginPage.enterEmail('abc123@gmail.com');
  });
});
