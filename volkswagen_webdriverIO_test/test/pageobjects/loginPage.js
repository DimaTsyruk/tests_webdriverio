import { equal } from 'assert';

class LoginPage {
  get emailInputField() {
    return $('input[id="input_email"]');
  }

  get nextButton() {
    return $('button[id="next-btn"]');
  }

  get errorEmailMessage() {
    return $('span[class="message"]');
  }

  async enterEmail(email) {
    await this.emailInputField.setValue(email);
    await this.nextButton.click();
  }

  async checkErrorEmailMessage() {
    const isElementExisting = await this.errorEmailMessage.isExisting();
    equal(isElementExisting, true, 'the error message is not displayed');
  }
}

export default new LoginPage();
