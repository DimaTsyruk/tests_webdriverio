import { equal } from 'assert';

class MenuPage {
  get modelsButton() {
    const elements = $$('a[class="StyledMenuLink-daAsYd fViWJp"]');
    return elements[0];
  }

  get builderButton() {
    const elements = $$('a[class="StyledMenuLink-daAsYd fViWJp"]');
    return elements[1];
  }

  get offersButton() {
    const elements = $$('a[class="StyledMenuLink-daAsYd fViWJp"]');
    return elements[4];
  }

  get myVWButton() {
    const elements = $$('a[class="StyledMenuLink-daAsYd fViWJp"]');
    return elements[6];
  }

  get contactUsButton() {
    const elements = $$('a[class="StyledMenuLink-daAsYd fViWJp"]');
    return elements[9];
  }

  get espanolButton() {
    const elements = $$('//span[contains(text(), "español")]');
    return elements[0];
  }

  async navigateToModelsPage() {
    await this.modelsButton.click();
  }

  async navigateToBuilderPage() {
    await this.builderButton.click();
  }

  async navigateToOffersPage() {
    await this.offersButton.click();
  }

  async navigateToMyVWPage() {
    await this.myVWButton.click();
  }

  async navigateToContactUsPage() {
    await this.contactUsButton.click();
  }

  async changeLanguage() {
    await this.espanolButton.click();
  }
}

export default new MenuPage();
