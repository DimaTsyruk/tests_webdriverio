import { equal } from 'assert';

class SearchPage {
  get searchBar() {
    return $('input[name="search-input"]');
  }

  get submitSearchButton() {
    return $('button[aria-label="Submit search"]');
  }

  get builtTiguanButton() {
    return $('a[aria-label="Tiguan: Build"]');
  }

  get headerText() {
    return $('h1[class="StyledTextComponent-sc-hqqa9q eGkMWo sc-iNmOIt laVPgs"]');
  }

  async searchProduct(name) {
    await this.searchBar.setValue(name);
    await this.submitSearchButton.click();
  }

  async builtTiguan() {
    await this.builtTiguanButton.click();
  }

  async verifyPage(keyWord) {
    const text = await this.headerText.getText();
    equal(text.includes('Tiguan'), true, 'the wrong page');
  }
}

export default new SearchPage();
