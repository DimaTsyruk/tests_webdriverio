import { equal } from 'assert';

class MainPage {
  async verifyURL(expectedUrl) {
    const actualUrl = await browser.getUrl();
    equal(actualUrl, expectedUrl, 'URL is incorrect');
  }
  async verifyURLbyKeyWord(keyWord) {
    const actualUrl = await browser.getUrl();
    equal(actualUrl.includes(keyWord), true, 'the wrong url');
  }
  async open() {
    await browser.url('https://www.vw.com/');
  }
}
export default new MainPage();
