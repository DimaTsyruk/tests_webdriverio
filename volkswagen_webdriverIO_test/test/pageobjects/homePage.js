import { equal } from 'assert';

class HomePage {
  get menuButton() {
    return $('button[aria-label="Menu"]');
  }

  get cookieCancelButton() {
    return $('a[id="ablehnTarget"]');
  }

  get magnifyingGlassButton() {
    const elements = $$('button[aria-label="Search"]');
    return elements[0];
  }

  get searchBar() {
    return $('input[name="search-input"]');
  }

  get submitSearchButton() {
    return $('button[aria-label="Submit search"]');
  }

  get userAccauntButton() {
    return $('button[aria-label="Login"]');
  }

  get LoginOrRegisterButton() {
    return $('a[title="Log in or register"]');
  }

  async cancelCookie() {
    await this.cookieCancelButton.click();
  }

  async navigateToMenu() {
    await this.menuButton.click();
  }

  async openSearchBar() {
    await this.magnifyingGlassButton.click();
  }

  async navigateToLoginPage() {
    await this.userAccauntButton.click();
    await this.LoginOrRegisterButton.click();
    await browser.pause(1000);
  }
}

export default new HomePage();
